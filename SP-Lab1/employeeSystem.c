/*
Author: Fan Guo
Date: 2020-11-1
Desc: 
	The Program implement an employee database program in C. 
	Program will store employee information in an array of employee structs, 
	sorted by employee ID value. And Program will read in employee data from 
	an input file when it starts-up.
	
	This project implements five major functions:
	- Print the Database
	- Lookup employee by ID 
	- Lookup employee by last name 
	- Add an Employee 
	- Quit


*/


#include "readfile.h"
#include <string.h>
#define MAXNUM 1024


char *filename = "";
int count = 0;
int maxId = 0;
struct Employee employees[MAXNUM];



// when employee sort by id, need cmp function
int cmp(const void *a, const void *b){
    int id1=((struct Employee *)a)->id;
    int id2=((struct Employee *)b)->id;
    return id1>id2 ? 1:-1;
}


// print employees sorted by id
void printData(){
	/*
	// Before sort
	printf("ID\t\tNAME\t\t\t\t\tSALARY\n");
	printf("---------------------------------------------------------------\n");
    for(int i = 0; i < count; i++)
    {
        printf("%d\t\t%s %s\t\t\t\t%d\n", employees[i].id, employees[i].first_name, employees[i].last_name,employees[i].salary);
    }
	printf("---------------------------------------------------------------\n");
	printf(" Number of Employees (%d)\n", count);
	*/
	
	// After sort
	qsort(employees,count,sizeof(struct Employee),cmp);
	printf("ID\t\tNAME\t\t\t\t\tSALARY\n");
	printf("---------------------------------------------------------------\n");
    for(int i = 0; i < count; i++)
    {
        printf("%d\t\t%s %s\t\t\t\t%d\n", employees[i].id, employees[i].first_name, employees[i].last_name,employees[i].salary);
    }
	printf("---------------------------------------------------------------\n");
	printf(" Number of Employees (%d)\n", count);

}


// Binary search employee by id
int BinarySearch(int id){
	int start = 0, end = count, mid;
	int result = -1;
	while(start <= end){
		mid = (start + end) / 2;
		if (employees[mid].id == id){
			result = mid;
			break;
		}else if(employees[mid].id >  id){
			end = mid - 1;
			continue;
			
		}else{
			start = mid + 1;
			continue;
		}
	}
	return result;	
}


// search employee by id
void searchById(){
	printf("Enter a 6 digit employee id: ");
	int id;
	read_int(&id);
	int findIndex = BinarySearch(id);
	if (findIndex != -1){
		 printf("ID\t\tNAME\t\t\t\t\tSALARY\n");
		 printf("---------------------------------------------------------------\n");
		 printf("%d\t\t%s %s\t\t\t\t%d\n", employees[findIndex].id, employees[findIndex].first_name, 
										   employees[findIndex].last_name,employees[findIndex].salary);	
		 printf("---------------------------------------------------------------\n");									   
	}else{
		printf("Employee with id %d not found in DB\n",id);
	}
}

// search employee by last_name
void searchByLastName(){
	printf("Enter Employee's last name (no extra spaces):");
	char last_name[MAXNAME];
	// scanf("%s", last_name);
	read_string(last_name);
	int found = 0;
	for(int i = 0; i < count; i++){
        if (strcmp(employees[i].last_name,last_name) == 0){
			 printf("ID\t\tNAME\t\t\t\t\tSALARY\n");
			 printf("---------------------------------------------------------------\n");
			 printf("%d\t\t%s %s\t\t\t\t%d\n", employees[i].id, employees[i].first_name, 
											   employees[i].last_name,employees[i].salary);	
			 printf("---------------------------------------------------------------\n");
			 found = 1;
			 return ;
		}
    }
	if (found == 0){
		printf("Not Found!\n");
		return ;
	}

}


// add employee 
void addData(){
	int id, salary, is_save = 0;
	// The six digit ID value must be between 100000 and 999999 inclusive.
	if (count == 0){
		id = 100000;
	}else{
		id = employees[count-1].id + 1;
	}
	char first_name[MAXNAME], last_name[MAXNAME];
	printf("Enter the first name of the employee:");
	// scanf("%s", first_name);
	read_string(first_name);
	printf("Enter the last name of the employee:");
	// scanf("%s", last_name);
	read_string(last_name);
	while(1){
		printf("Enter employee's salary (30000 to 150000): ");
		// scanf("%d", &salary);
		read_int(&salary);
		// Salary amounts must be between $30,000 and $150,000 inclusive.
		if (salary >= 30000 &&  salary <= 150000){
			break;
		}else{
			printf("Salary amounts must be between $30,000 and $150,000 inclusive, Retype salary!!!\n");
		}
	}	
	employees[count].id = id;
	strcpy(employees[count].first_name, first_name);
	strcpy(employees[count].last_name, last_name);	
	employees[count].salary = salary;
	count ++;
	printf("do you want to add the following employee to the DB?\n");
	printf("\t\t\t%d %s %s,  salary: %d\n", id, first_name, last_name,salary);
	printf("Enter 1 for yes, 0 for no: ");
	// scanf("%d", &is_save);
	read_int(&is_save);
	if (is_save == 1){
		printf("save into DB\n");
		FILE *fp;
		if((fp = fopen(filename, "w")) == NULL){
			fprintf(stderr, "Can't open the %s", filename);
		}else{
			printf("%d\n", count);
			for(int i; i < count; i++){
				fprintf(fp, "%d\t%s\t%s\t%d\n", employees[i].id, employees[i].first_name, employees[i].last_name, employees[i].salary);
			
			}
		}		
	}
}


// print out a menu of transaction options
void menu(){
	printf("Employee DB Menu:\n");
	printf("----------------------------------\n");
	printf("\t\t(1) Print the Database\n");
	printf("\t\t(2) Lookup by ID\n");
	printf("\t\t(3) Lookup by last name\n");
	printf("\t\t(4) Add an Employee\n");
	printf("\t\t(5) Quit\n");
	printf("----------------------------------\n");
	printf("Enter your choice:");
}


// handle the user's transaction choice
void keyDown(){
	int userKey;
	int ret = read_int(&userKey);
	if (ret == -1){
		userKey = 0;
	}
	switch(userKey){
	case 1:
		printf("\n\n\t\tPrint the Database\n\n");
		printData();
		break;
	case 2:
		 printf("\n\n\t\tLookup employee by ID\n");
		 searchById();
         break;
	case 3:
		 printf("\n\n\t\tLookup employee by Last Name\n");
		 searchByLastName();
         break;
	case 4:
		printf("\n\n\t\tAdd an Employee\n");
		addData();
        break;
	case 5:
		printf("\n\n\t\tExiting Employee System.....\n");
		printf("\t\tgoodbye!\n");
		exit(0);
		break;
	default:
		printf("Hey, %d is not between 1 and 5, try again...\n", userKey);
		break;
	}
}



// main function
int main(int argc, char *argv[]){
	// if not give filename(small.txt), print usage and exit
	if (argc != 2){
		fprintf(stderr, "Usage: ./workerDB small.txt\n");
		exit(1);
	}
	// if give filename(small.txt), read file about employees data,show system menu and get user choice
	filename = argv[1];
	read_file(filename, employees, &count);
	while(1){
		menu();
		keyDown();
	}
	return 0;
	
}

