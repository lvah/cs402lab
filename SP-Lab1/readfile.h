#include <stdio.h>
#include <stdlib.h>
#include "employee.h"
int read_file(char *filename, struct Employee *employees, int *count);
int save_file(char *filename, struct Employee *employees, int *count);
int read_float(float *f);
int read_int(int *x);
int read_string(char *s);
