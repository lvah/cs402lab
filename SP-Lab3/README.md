## Employee Management  System
BitBucket Link: https://bitbucket.org/lvah/cs402lab/src/master/SP-Lab3


This program implement that reads data from a file and computes some basic statistical analysis measures for the data.

1. reads data from a file, The array of values must be dynamically allocated.Start out allocating an array of 20 float value.As you read in values into the current array, if you run out of capacity
	
	1). Call malloc to allocate space for a new array that is twice the size of the current full one.
	
	2). Copy values from the old full array to the new array (and make the new array the current one).
	
	3). Free the space allocated by the old array by calling free.
	
2. computes some basic statistical analysis:
	
	1). Num values
	
	2). mean: the average of values in the set. 
		For example, if the set is S={5, 6, 4, 2, 7}, then the average is 4.8
	
	3). median: the middle value in the set of values.
		For example, if the set is S={5, 6, 4, 2, 7},then the median value is 5 (2 and 4 are smaller and 6 and 7 are larger). 
		If the set has an even number of values, then you average the two values that are left and right of center.
	
	4). stddev: the population standard deviation by formula.  stddev = sqrt((sum((xi - mean)^2))/N);	
	
	5). Unused array capacity

## Step to run

1.Open the program directory, The project contains the following file structure:
```
.
├── basicstats          # execute program
├── main.c				# source main code
├── README.md			# README.md,learn how to run program
├── large.txt			# sample file
└── small.txt			# sample file

```

2.execute program
```bash
$ ./basicstats  small.txt 
Results:
----------
Num values: 		        12
      mean: 		    85.776
    median: 		    67.470
    stddev: 		    90.380
Unused array capacity:	     8


$ ./basicstats large.txt 
Results:
----------
Num values: 		        30
      mean: 		  2035.600
    median: 		  1956.000
    stddev: 		  1496.153
Unused array capacity:	    10

```

If you want to compile source code, use shell:
```bash
gcc -o basicstats main.c  -lm
./basicstats  small.txt
``` 






