/*
Author: Fan Guo
Date: 2020-11-15
Desc:  

This program implement that reads data from a file and computes some basic statistical analysis measures for the data.

1. reads data from a file, The array of values must be dynamically allocated.
Start out allocating an array of 20 float value.
As you read in values into the current array, if you run out of capacity
	1). Call malloc to allocate space for a new array that is twice the size of the current full one.
	2). Copy values from the old full array to the new array (and make the new array the current one).
	3). Free the space allocated by the old array by calling free.
	
2. computes some basic statistical analysis:
	1). Num values: 
	2). mean: the average of values in the set. 
		For example, if the set is S={5, 6, 4, 2, 7}, then the average is 4.8
	3). median: the middle value in the set of values.
		For example, if the set is S={5, 6, 4, 2, 7},then the median value is 5 (2 and 4 are smaller and 6 and 7 are larger). 
		If the set has an even number of values, then you average the two values that are left and right of center.
	4). stddev: the population standard deviation by formula.  stddev = sqrt((sum((xi - mean)^2))/N);	
	5). Unused array capacity



*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

// function to open file
FILE *open_file(char *filename){
	if(fopen(filename, "r") == NULL){
		printf("Error reading file\n");
		exit(0);
	}	
}

// function to get mean
double get_mean(float *p, int n){
	double sum = 0;
	for(int i=0; i<n; i++){
		sum = sum + p[i];
	}
	double mean = sum/n;
	return mean;
} 

// function to get sd
double get_sd(float *p, int n){
	// sd = sqrt((sum((xi-mean)^2)))/n
	double sd = 0;
	double mean = get_mean(p, n);
	for(int i=0; i<n; i++){
		sd += pow(p[i]-mean, 2);	
	}
	return sqrt(sd/n);
}


// function to get median
double get_median(float *cur_arr, int n){
	if(n%2 == 1){
		return cur_arr[n/2];
	}else{
		double min = cur_arr[n/2-1];
		double max = cur_arr[n/2];
		return (min+max)/2;
	}
}

// function to sort array
void swap(float *xp, float *yp){
	float temp = *xp;
	*xp = *yp;
	*yp = temp;
}


// function to sort
float sort(float *array, int n){
	int i, j, min_idx;
	for(i=0; i< n-1; i++){
		for(j=0; j<n-i-1; j++){
			if (array[j] > array[j+1])
				swap(&array[j], &array[j+1]);
		}
	}
}




// main function
int main(int argc, char *argv[]){
	// if not give filename(small.txt), print usage and exit
	if (argc != 2){
		fprintf(stderr, "Usage: ./basicstats small.txt|large.txt\n");
		exit(1);
	}
	// if give filename, do task.
	char *filename = argv[1];
	int n = 20, length_arr = 0;
	float temp;
	float *cur_arr = (float *)malloc(n*sizeof(float));
	FILE *fp = open_file(filename);
	
	while(!feof(fp)){
		fscanf(fp, "%f\n", (cur_arr+length_arr));
		length_arr++;
		// the current length of array is equal to max capacity.
		if(length_arr == n){
			// Create new array with twice size.
			float *new_arr = (float *)malloc(2*n*sizeof(float));
			// Copy values from the okd temp array to new array.
			memcpy(new_arr, cur_arr, length_arr * sizeof(float));
			// free memory
			free(cur_arr);
			cur_arr = new_arr;
			// increase the size of n.
			n = n * 2;
		}
	}
	fclose(fp);
	double mean = get_mean(cur_arr, length_arr);
	double sd = get_sd(cur_arr, length_arr);
	sort(cur_arr, length_arr);
	double median = get_median(cur_arr, length_arr);
	printf("\033[32mResults:\033[0m\n");
	printf("----------\n");
	printf("Num values: 		%10d\n", length_arr);
	printf("      mean: 		%10.3f\n", mean);
	printf("    median: 		%10.3f\n", median);
	printf("    stddev: 		%10.3f\n", sd);
	printf("Unused array capacity:	%10d\n", n-length_arr);
	return 0;
	
}

