#define MAXNAME 64
#define MAXNUM 1024

// define an employee struct
struct Employee{
    int id; 
    char first_name[MAXNAME];
    char last_name[MAXNAME];
    int salary;
};

