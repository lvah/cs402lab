## Employee Management  System
BitBucket Link: https://bitbucket.org/lvah/cs402lab/src/master/SP-Lab2/README.md


This System implement an employee database program in C programming.There has 3 important things.


- Firstly, store employee information in an array of employee structs.
- Secondly, sorted by employee ID value. 
- Last but not least, read in employee data from an input file when it starts-up.

In Version 1, This project implements five major functions:

- Print the Database
- Lookup employee by ID 
- Lookup employee by last name 
- Add an Employee 
- Quit

In Version 2, The project implements add four major functions:

- Remove an employee
- Update an employee's information
- Print the M employees with the highest salaries
- Find all employees with matching last name(The matching is case insensitive)

## Step to run

1.Open the program directory, The project contains the following file structure:
```
.
├── employee.h			   // define employee struc
├── employeeSystem.c       // main program
├── readfile.h			   // library about read file, read int, read char, read float
├── readfile.c			   // code about read file, read int, read char, read float
├── README.md			   // program documentation
├── small.txt              // db file
└── workDB				   // execute program
```

2.execute program
```bash
./workDB small.txt
```

If you want to compile source code, use shell:
```bash
gcc *.c -o workDB
./workDB small.txt
``` 






