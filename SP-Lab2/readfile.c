/*
Author: Fan Guo
Date: 2020-11-1
Desc: 
	 simplify reading data from the input file.


*/

#include <stdio.h>
#include <stdlib.h>
#include "employee.h"
int read_file(char *filename, struct Employee *employees, int *count){
	FILE *fp;
	if((fp = fopen(filename, "r")) == NULL){
        fprintf(stderr, "Can't open the %s", filename);
		return -1;
    }
	 for(int i = 0; i < MAXNUM; i++){
        if((fscanf(fp, "%s\t%s\t\t%d\t%d",  employees[i].first_name, employees[i].last_name,&employees[i].salary, &employees[i].id)) != 4)
        {
			fprintf(stderr, "File format error");
			*count = i;
            break;
        }
    }
	return 0;
	
}



int read_float(float *f){
	int ret = scanf("%f", f);
	if (ret == 1){
		return 0;
	}else{
		return -1;
	}
}

int read_int(int *x){
    int ret = scanf("%d", x); 
    if (ret == 1){ 
        return 0;
    }else{
        return -1;
    }   
}

int read_string(char *s){
    int ret = scanf("%s", s);
    if (ret == 1){
        return 0;
    }else{
        return -1;
    }
}


