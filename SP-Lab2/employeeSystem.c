/*
Author: Fan Guo
Date: 2020-11-1
Desc: 
	The Program implement an employee database program in C. 
	Program will store employee information in an array of employee structs, 
	sorted by employee ID value. And Program will read in employee data from 
	an input file when it starts-up.
	
	This project implements five major functions:
	- Print the Database
	- Lookup employee by ID 
	- Lookup employee by last name 
	- Add an Employee 
	- Quit


*/


#include "readfile.h"
#include <string.h>
#define MAXNUM 1024


char *filename = "";
int count = 0;
struct Employee employees[MAXNUM];



// when employee sort by id, need cmp function
int cmp(const void *a, const void *b){
    int id1=((struct Employee *)a)->id;
    int id2=((struct Employee *)b)->id;
    return id1>id2 ? 1:-1;
}


// print employees sorted by id
void printData(){
	// After sort
	qsort(employees,count,sizeof(struct Employee),cmp);
	printf("NAME\t\t\tSALARY\tID\n");
	printf("---------------------------------------------------------------\n");
    for(int i = 0; i < count; i++)
    {
        printf("%-10s%-10s\t%d\t%d\n",  employees[i].first_name, employees[i].last_name,employees[i].salary, employees[i].id);
    }
	printf("---------------------------------------------------------------\n");
	printf(" Number of Employees (%d)\n", count);

}


// Binary search employee by id
int BinarySearch(int id){
	int start = 0, end = count, mid;
	int result = -1;
	while(start <= end){
		mid = (start + end) / 2;
		if (employees[mid].id == id){
			result = mid;
			break;
		}else if(employees[mid].id >  id){
			end = mid - 1;
			continue;
			
		}else{
			start = mid + 1;
			continue;
		}
	}
	return result;	
}


// search employee by id
int searchById(){
	printf("Enter a 6 digit employee id: ");
	int id;
	read_int(&id);
	int findIndex = BinarySearch(id);
	if (findIndex != -1){	
		printf("NAME\t\t\tSALARY\tID\n");
		printf("---------------------------------------------------------------\n");
		printf("%-10s%-10s\t%d\t%d\n",  employees[findIndex].first_name, employees[findIndex].last_name,
										  employees[findIndex].salary, employees[findIndex].id);	
		printf("---------------------------------------------------------------\n");
		return findIndex;
	}else{
		printf("Employee with id %d not found in DB\n",id);
		return -1;
	}
}

// search employee by last_name
void searchByLastName(){
	printf("Enter Employee's last name (no extra spaces):");
	char last_name[MAXNAME];
	// scanf("%s", last_name);
	read_string(last_name);
	int found = 0;
	for(int i = 0; i < count; i++){
        if (strcmp(employees[i].last_name,last_name) == 0){
			 printf("NAME\t\t\tSALARY\tID\n");
			 printf("---------------------------------------------------------------\n");
			 printf("%-10s%-10s\t%d\t%d\n",  employees[i].first_name, employees[i].last_name,
											   employees[i].salary, employees[i].id);	
			 printf("---------------------------------------------------------------\n");
			 found = 1;
			 return ;
		}
    }
	if (found == 0){
		printf("Employee with lastname = %s not found in DB\n", last_name);
		return ;
	}

}


// save_file
void save_file(){
	FILE *fp;
	if((fp = fopen(filename, "w")) == NULL){
		fprintf(stderr, "Can't open the %s", filename);
	}else{
		for(int i=0; i < count; i++){
			fprintf(fp, "%-10s%-10s\t%d\t%d\n", employees[i].first_name, employees[i].last_name, employees[i].salary, employees[i].id);
		}
	}
}


// add employee 
void addData(){
	int id, salary, is_save = 0;
	// The six digit ID value must be between 100000 and 999999 inclusive.
	if (count == 0){
		id = 100000;
	}else{
		id = employees[count-1].id + 1;
	}
	char first_name[MAXNAME], last_name[MAXNAME];
	printf("Enter the first name of the employee:");
	// scanf("%s", first_name);
	read_string(first_name);
	printf("Enter the last name of the employee:");
	// scanf("%s", last_name);
	read_string(last_name);
	while(1){
		printf("Enter employee's salary (30000 to 150000): ");
		// scanf("%d", &salary);
		read_int(&salary);
		// Salary amounts must be between $30,000 and $150,000 inclusive.
		if (salary >= 30000 &&  salary <= 150000){
			break;
		}else{
			printf("Salary amounts must be between $30,000 and $150,000 inclusive, Retype salary!!!\n");
		}
	}	
	employees[count].id = id;
	strcpy(employees[count].first_name, first_name);
	strcpy(employees[count].last_name, last_name);	
	employees[count].salary = salary;
	
	printf("do you want to add the following employee to the DB?\n");
	printf("\t\t\t%d %s %s,  salary: %d\n", id, first_name, last_name,salary);
	printf("Enter 1 for yes, 0 for no: ");
	// scanf("%d", &is_save);
	read_int(&is_save);
	if (is_save == 1){
		printf("save into DB\n");
		count ++;
		save_file();

	}
}


// Remove an employee
void remove_employee(){
	// base searchById function,get remove employee id index.
	int findIndex = searchById();
	if (findIndex != -1){
		printf("\tEmployee in the DB have been found, do you wants to remove?\n");
		printf("Enter 1 for yes, 0 for no: ");
		int is_remove;
		read_int(&is_remove);
		if (is_remove == 1){
			count --;
			printf("\nEmployee is removing!\n");
			// move info
			for(int i = findIndex; i < count; i++){
				employees[i] = employees[i+1];
				employees[i].id = employees[i+1].id;
				employees[i].salary = employees[i+1].salary;
				stpcpy(employees[i].first_name, employees[i+1].first_name);
				stpcpy(employees[i].last_name, employees[i+1].last_name);				
			}
			save_file();	
		}
	}
}

// Update an employee
void update_employee(){
	// base searchById function,get remove employee id index.
	int findIndex = searchById();
	if (findIndex != -1){
		printf("Updating %d employee info \n", employees[findIndex].id);
		// if 100000 <= new_id < = 999999 and new_id not in db,update db. else retype new_id.
		while(1){
			printf("Enter new ID(old=%d): ", employees[findIndex].id);
			int new_id;
			read_int(&new_id);
			if(new_id < 100000 || new_id > 999999){
				printf("ID number out of bound, please try again!\n");
			}else{
				int find_new_index = BinarySearch(new_id);
				if (find_new_index == -1){
					employees[findIndex].id = new_id;
					break;
				}else{
					printf("ID number is exist, please try again!\n");
						
				}
				
			}
		}
		printf("Enter new first_name(old=%s):", employees[findIndex].first_name);
		read_string(employees[findIndex].first_name);
		printf("Enter new last_name(old=%s):", employees[findIndex].last_name);
		read_string(employees[findIndex].last_name);
		
		while(1){
			printf("Enter new employee's salary (30000 to 150000): ");
			int salary;
			// scanf("%d", &salary);
			read_int(&salary);
			// Salary amounts must be between $30,000 and $150,000 inclusive.
			if (salary >= 30000 &&  salary <= 150000){
				employees[findIndex].salary = salary;
				save_file();
				printf("Update %d Success!\n", employees[findIndex].id);
				break;
			}else{
				printf("Salary amounts must be between $30,000 and $150,000 inclusive, Retype salary!!!\n");
			}
		}	

	}
}

// cmp with salary, from big to small
int cmp_with_salary(const void *a, const void *b){
    int id1=((struct Employee *)a)->salary;
    int id2=((struct Employee *)b)->salary;
    return id1<id2 ? 1:-1;
}

// Print the M employees with the highest salaries
void highestSalaries(){
	int m;
	while(1){
		printf("Enter M = ");
		read_int(&m);
		if (m < 0 || m > count){
			printf("M is bigger than employee count or less than 0. Retype M!\n");
		}else{
			break;
		}
	}
	qsort(employees,count,sizeof(struct Employee),cmp_with_salary);
	printf("NAME\t\t\tSALARY\tID\n");
	printf("---------------------------------------------------------------\n");
    for(int i = 0; i < m; i++)
    {
        printf("%-10s%-10s\t%d\t%d\n",  employees[i].first_name, employees[i].last_name,employees[i].salary, employees[i].id);	
    }
	printf("---------------------------------------------------------------\n");
	printf(" Number of Employees (%d)\n", m);
}


// return information about every employee with a matching last name. The matching should be case insensitive.
void searchAllByLastName(){
	printf("Enter Employee's last name (no extra spaces):");
	char last_name[MAXNAME];
	// scanf("%s", last_name);
	read_string(last_name);
	
	struct Employee temp[MAXNUM];
	
	int found = 0;
	for(int i = 0; i < count; i++){
		// The matching should be case insensitive.
        if (strcasecmp(employees[i].last_name,last_name) == 0){
			temp[found] = employees[i];
			found ++;
		}
    }
	if (found == 0){
		printf("Not Found!\n");
		return ;
	}else{
		printf("NAME\t\t\tSALARY\tID\n");
		printf("---------------------------------------------------------------\n");
		for(int i=0; i < found; i++){
			printf("%-10s%-10s\t%d\t%d\n", temp[i].first_name, temp[i].last_name,temp[i].salary, temp[i].id);	
		}
		printf("---------------------------------------------------------------\n");
		printf(" Number of Employees (%d)\n", found);
	}

}


// print out a menu of transaction options
void menu(){
	printf("\n\nEmployee DB Menu:\n");
	printf("----------------------------------\n");
	printf("\t\t(1) Print the Database\n");
	printf("\t\t(2) Lookup by ID\n");
	printf("\t\t(3) Lookup by last name\n");
	printf("\t\t(4) Add an Employee\n");
	printf("\t\t(5) Quit\n");
	printf("\t\t(6) Remove an employee\n");
	printf("\t\t(7) Update an employee's information\n");
	printf("\t\t(8) Print the M employees with the highest salaries\n");
	printf("\t\t(9) Find all employees with matching last name\n");
	printf("----------------------------------\n");
	printf("Enter your choice:");
}


// handle the user's transaction choice
void keyDown(){
	int userKey;
	int ret = read_int(&userKey);
	if (ret == -1){
		userKey = 0;
	}
	switch(userKey){
	case 1:
		printf("\n\n\t\tPrint the Database\n\n");
		printData();
		break;
	case 2:
		 printf("\n\n\t\tLookup employee by ID\n");
		 searchById();
         break;
	case 3:
		 printf("\n\n\t\tLookup employee by Last Name\n");
		 searchByLastName();
         break;
	case 4:
		printf("\n\n\t\tAdd an Employee\n");
		addData();
        break;
	case 5:
		printf("\n\n\t\tExiting Employee System.....\n");
		printf("\t\tgoodbye!\n");
		exit(0);
		break;
	case 6:
		printf("\n\n\t\tRemove Employee .....\n");
		remove_employee();
		break;
	case 7:
		printf("\n\n\t\tUpdate Employee .....\n");
		update_employee();
		break;	
	case 8:
		printf("\n\n\t\tPrint the M employees with the highest salaries...\n");
		highestSalaries();
		break;
	case 9:
		printf("\n\n\t\tFind all employees with matching last name...\n");
		searchAllByLastName();
		break;
	default:
		printf("Hey, %d is not between 1 and 6, try again...\n", userKey);
		break;
	}
}



// main function
int main(int argc, char *argv[]){
	// if not give filename(small.txt), print usage and exit
	if (argc != 2){
		fprintf(stderr, "Usage: ./workerDB small.txt\n");
		exit(1);
	}
	// if give filename(small.txt), read file about employees data,show system menu and get user choice
	filename = argv[1];
	// reading file that is not guaranteed to be in sorted order by employee ID
	read_file(filename, employees, &count);
	
	while(1){
		menu();
		keyDown();
	}
	return 0;
	
}

