#this is a program used to test memory alignment for data

.data 0x10000000
user1:		.word 0
msg1:		.ascii "Please enter an interger number:"
msg2:		.ascii "If bytes were layed in reverse order the number would be:"

	.text	
	.globl main	
main:	
	add $sp, $sp, -4 	# save $ra in $sp
	sw $ra, 4($sp)
	la $a0, msg1
	li $v0, 4
	syscall
	
	li $v0, 5
	syscall
	
	sw $v0, user1
	la $a0, user1
	jal Reverse_bytes
	la $a0, msg2
	li $v0, 4
	syscall
	
	lw $t5, user1
	li $v0, 1
	move $a0, $t5
	syscall
	
	lw $ra, 4($sp) 	# restore the return address
	add $sp, $sp, 4
	jr $ra			# return from main
	
Reverse_bytes:
		lb $t1, 0($a0)
		lb $t2, 1($a0)
		lb $t3, 2($a0)
		lb $t4, 3($a0)		
		sb $t4, 0($a0)
		sb $t3, 1($a0)
		sb $t2, 2($a0)
		sb $t1, 3($a0)	
		jr $ra
	
	
	
	
	
	
	
	
	
	
	
	
	





jr $ra				# return from main