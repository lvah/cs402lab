	.data
		input_msg: 	.asciiz		"Please enter two non-negative integers:"	
		output_msg:	.asciiz		"Answer is: "
		error_msg:	.asciiz		"One or more numbers is negative.Try again."
	.text
	.globl main
main:  	
		li $v0, 4			# display input_msg
		la $a0, input_msg
		syscall
		li $v0, 5			# system call for read_int
		syscall
		move $t0, $v0		# first integer
		li $v0, 5			# system call for read_int
		syscall
		move $t1, $v0		# second integer
		
		# check whether input is negative
		bltz $t0, error
		bltz $t1, error
		j ok
error:
		li $v0, 4			# display error_msg
		la $a0, error_msg
		syscall
		j main
		
ok:		

		li $v0, 4			# display output_msg
		la $a0, output_msg
		syscall
		
		addi $sp, $sp, -4 	# save $ra
		sw $ra, 4($sp) 		
		move $a0, $t0		# pass parameter
		move $a1, $t1		# pass parameter
		jal Ackermann
		
		move $a0, $v0		# move return value because syscall also used
		li $v0, 1			# print int 
		syscall
		lw $ra, 4($sp)		# restore $ra
		addi $sp, $sp, 4	
		jr $ra
		
		

Ackermann:
		# if x = 0 then y+1	
		beq $a0, $0,  x_0			
		addi $sp, $sp, -4		
		sw $ra, 4($sp)			
	
		# if y = 0, then A(x-1, 1)	
		beq $a1, $0,  y_0		

		# else: A(x-1, A(x, y-1))
		addi $t0, $a0, -1		# x-1
		addi $sp, $sp, -4		# store x-1 		
		sw $t0, 4($sp)	
		addi $a1, $a1, -1		# y-1				
		jal Ackermann			
	
		lw $a0, 4($sp)			
		addi $sp, $sp, 4
		move $a1, $v0
		jal Ackermann			# A(x-1, A(x, y-1))
		j end				
x_0:							# if x == 0, then y+1
		addi $v0, $a1, 1		
		jr $ra
y_0:							# y == 0
		addi $a0, $a0, -1		# x-1
		li $a1, 1				# 1
		jal Ackermann			# Ackermann(x-1, 1)

end:
		lw $ra, 4($sp)			
		addi $sp, $sp, 4		
		# adjust the stack pointer
		jr $ra			
				
		
	