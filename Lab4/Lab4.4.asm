		.data 
msg:	.ascii	"The largest number is:"		
		
		.text
		.globl main
main:  	
		# read int
		li $v0, 5
		syscall
		move $t0, $v0
		li $v0, 5
		syscall
		move $t1, $v0		
		
		addi $sp, $sp, -4 	# save $ra
		sw $ra, 4($sp) 		# push $ra into the stack(save $ra in allocated stack space)
		
		# The parameters for Largest are all passed on the stack insted of register.
		addi $sp, $sp, -8 	# allocate stack space for 2 words
		sw $t0, 4($sp)		# push arguments into stack
		sw $t1, 8($sp)
		
		jal Largest 		# call ‘Largest’ with parameters
		addi $sp, $sp, 8	# restore $sp position
		lw $ra, 4($sp) 		# restore $ra from stack
		addi $sp, $sp, 4 	# free stack space
		jr $ra 				# return from main

Largest:
		li $v0, 4
		la $a0, msg
		syscall
		
		# The parameters for Largest are all passed on the stack insted of register.
		lw $a0, 4($sp)
		lw $a1, 8($sp)
		
		slt $t0, $a0, $a1
		blez $t0, output	# if $t0 <= 0($a0>=$a1), goto output
		move $a0, $a1       # $a0 < $a1, we should output $a1
output:
			li $v0, 1		# print largest number
			syscall
			jr $ra
