		.text
		.globl main
main:  	
		addi $sp, $sp, -4 	# first update the stack pointer(substract 4 from stack pointer)
		sw $ra, 4($sp) 		# push $ra into the stack(save $ra in allocated stack space)
		move $s0, $ra 		# must save $ra since I’ll have a call
		jal test 			# call ‘test’ with no parameters
		nop 				# execute this after ‘test’ returns
		lw $ra, 4($sp) 		# first recover the data from the stack(restore $ra from stack)
		addi $sp, $sp, 4 	# free stack space
		jr $ra 				# return from main

		
		
#The procedure ‘test’ does not call any other procedure. Therefore $ra
#does not need to be saved. Since ‘test’ uses no registers there is
# no need to save any registers.
test:	
		nop					# this is the procedure named ‘test’
		jr $ra				# return from this procedure