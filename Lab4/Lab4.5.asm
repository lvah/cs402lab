	.data
		input_msg: 	.asciiz		"Please enter a integer number:"	
		output_msg:	.asciiz		"Factorial of input number is: "
		error_msg:	.asciiz		"Input is negative.Try again."
	.text
	.globl main
main:  	
		li $v0, 4			# display input_msg
		la $a0, input_msg
		syscall
		li $v0, 5			# system call for read_int
		syscall
		move $t0, $v0
		bgez $t0, next		# if input >= 0, proceed, do next
		li $v0, 4			# display error_msg
		la $a0, error_msg
		syscall
		j main
		
next:		
		addi $sp, $sp, -4 	# save $ra
		sw $ra, 4($sp) 		
		move $a0, $t0		# pass parameter
		jal Factorial
		move $t0, $v0		# move return value because syscall also used
		li $v0, 4
		la $a0, output_msg
		syscall
		li $v0, 1			# print int 
		move $a0, $t0
		syscall
		lw $ra, 4($sp)		# restore $ra
		addi $sp, $sp, 4	
		jr $ra
		
		

		
Factorial:
		addi $sp, $sp, -4
		sw $ra, 4($sp)			# save the return address on stack
		beqz $a0, terminate		# test for termination
		addi $sp, $sp, -4		# do not terminate yet
		sw $a0, 4($sp)			# save the parameter
		sub $a0, $a0, 1			# will call with a smaller argument
		jal Factorial
		#after the termination condition is reached these lines
		#will be executed
		lw $t0, 4($sp)			# the argument I have saved on stack
		mul $v0, $v0, $t0		# do the multiplication
		lw $ra, 8($sp)			# prepare to return
		addu $sp, $sp, 8		# I’ve popped 2 words (an address and
		jr $ra					# .. an argument)

terminate:
		li $v0, 1				# 0! = 1 is the return value
		lw $ra, 4($sp)			# get the return address
		addu $sp, $sp, 4		
		# adjust the stack pointer
		jr $ra			
				
		
	