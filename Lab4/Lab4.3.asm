		.data 
msg:	.ascii	"The largest number is:"		
		
		.text
		.globl main
main:  	
		# read int
		li $v0, 5
		syscall
		move $t0, $v0
		li $v0, 5
		syscall
		move $t1, $v0		
		
		addi $sp, $sp, -4 	# first update the stack pointer(substract 4 from stack pointer)
		sw $ra, 4($sp) 		# push $ra into the stack(save $ra in allocated stack space)
		move $a0, $t0		# pass parameters
		move $a1, $t1 		# must save $ra since I’ll have a call
		jal Largest 		# call ‘Largest’ with parameters
		lw $ra, 4($sp) 		# first recover the data from the stack(restore $ra from stack)
		addi $sp, $sp, 4 	# free stack space
		jr $ra 				# return from main

Largest:
		move $t0, $a0		# move $a0 temporarily since syscall needs
		li $v0, 4
		la $a0, msg
		syscall
		move $a0, $t0
		slt $t0, $a0, $a1
		blez $t0, output	# if $t0 <= 0($a0>=$a1), goto output
		move $a0, $a1       # $a0 < $a1, we should output $a1
output:
			li $v0, 1		# print largest number
			syscall
			jr $ra
