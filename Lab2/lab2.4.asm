.data 0x10010000 

var1:  .word 10        # var1 is a word (32 bit) with the initial value 10. 

                       
var2:  .word 03        # var2 is a word (32 bit) with the initial value 03.
 
.extern ext1 4

.extern ext2 4  


       .text 
       .globl main 

main:  addu $s0, $ra, $0  # save $31 in $16 

       lw $t0, var1       # load var1 to $t0
       lw $t1, var2       # load var2 to $t1
      
       sw $t0, ext1       # store the value in register t3 to var1
       sw $t1, var2       # store the value in register t2 to var2
    
                          # restore now the return address in $ra and return from main
 
      addu $ra, $0, $s0   # return address back in $31 
      jr $ra              # return from main





