.data 
msg1: .asciiz "I'm far away"
msg2: .asciiz "I'm nearby"
msg3: .asciiz "Please enter an integer number: "
far:  .word 1

.text
.globl main
main:	
		li $v0, 4 							# system call for print_str
		la $a0, msg3 						# address of string to print
		syscall
		li $v0, 5							# syscall #5: read_int
		syscall
		move $t0, $v0						# the value read is in $v0	
		
		
		li $v0, 4 							# system call for print_str
		la $a0, msg3 						# address of string to print
		syscall
		li $v0, 5							# syscall #5: read_int
		syscall
		move $t1, $v0
		
		
		beq $t0, $t1, Far
		li $v0, 4							# syscall #4: print_string
		la $a0, msg2
		syscall
		
		li $v0, 10
		syscall
Far:	
		li $v0, 4
		la $a0, msg1
		syscall
		b far
		jr $ra
		