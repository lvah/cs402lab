.data 
# var1 is a word (32 bit) with the initial value 4
var1:		.word 4	
var2:		.word 7
var3:		.word -2020

		.text
		.globl main
main:	
	lw $t0, var1($0)		# $t0 <- var1
	lw $t1, var2($0)		# $t1 <- var2
	slt $t2, $t1, $t0		# if($t1<$t2) $t0=1; else $t0=0
	blez $t2, else			# jump at label if $t0 is greater than zero
	
	
	lw $t2, var3($0)
	sw $t2, var1($0)
	sw $t2, var2($0)
	beq $0, $0, final		# go to final label unconditionally
	
else:
	move $t2, $t0 		# swap $t0 and $t1, $t2 is the temp variable
	move $t0, $t1
	move $t1, $t2
	sw $t0, var1($0)	# save new value to their address
	sw $t1, var2($0)		
	
final:
	li $v0, 1			# system call #1 - print int
	lw $a0, var1($0)	
	syscall
	li $v0, 1
	lw $a0, var2($0)
	syscall
	jr $ra
	
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			


# restore now the return address in $ra and return from main 
	addu $ra, $0, $s0	# return address back in $31 
	jr $ra			# return from main
