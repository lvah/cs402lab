.data 0x10010000
var1:		.word 2			# var1 is a word (32 bit) with the initial value 2

.text
.globl main
main:	
		lw $t0, var1($0)	# load var1 to register a0	
		li $a1 100			# $a1 <- 100		
		move $t1, $t0		# i is in $t0
loop:	ble $a1, $t1, exit	# exit if limit<=i
		addi $t0, $t0, 1
		addi $t1, $t1, 1	# if($t0 > 0)
		j loop
exit:
		sw $t0, var1($0)
		li $v0, 1			# syscall #1: print_int
		lw $a0, var1($0)
		syscall 
		jr $ra			# return from main
